import commentsController from '../controllers/comments.controller';
import * as express from 'express';
const router = express.Router()

router.get("/posts/:postId", commentsController.getCommentsForPost);
router.post("/", commentsController.createComment);
router.put("/:id", commentsController.updateComment);
router.delete("/:id", commentsController.deleteComment)
router.get("/:id", commentsController.getCommentById)

export default router

import postsController from '../controllers/posts.controller';
import * as express from 'express';
const router = express.Router()

router.get("/", postsController.getAllPosts);
router.get("/:id", postsController.getPostById)
router.get("/:authorId/author", postsController.getAllPostsByAuthor)
router.post("/", postsController.createPost);
router.put("/", postsController.updatePost);
router.delete("/:id", postsController.deletePost)


export default router

export default class Comment {
    id: number;
    postId: number;
    parentId?: number;
    content: string;
    authorId: number;
    createdAt?: string;
    updatedAt?: string;

    constructor(postId: number, content: string, authorId: number, parentId = 0) {
        this.postId = postId;
        this.content = content;
        this.authorId = authorId;
        this.parentId = parentId;
        this.createdAt = new Date().toISOString();
    }
}
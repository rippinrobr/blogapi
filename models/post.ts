export default class Post {
    id: number;
    title: string;
    content: string;
    authorId: number;
    createdAt?: string;
    updatedAt: string;

    constructor(title: string, content: string, authorId: number, createdAt = null, updatedAt = null, id = null) {
        this.title = title;
        this.content = content;
        this.authorId = authorId; 
        this.id = id || 0;
        this.updatedAt = updatedAt || "";

        if (createdAt === null) {
            let now = new Date()
            this.createdAt = now.toISOString();
        } 
    }
}

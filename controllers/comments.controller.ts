import repo from '../repositories/comment.repository';
import postRepo from '../repositories/post.repository'
import { Request, Response } from 'express'
import Comment from '../models/comment';
import {returnIdMustBeANumber, returnInvalidRequest, returnNotFound, returnServerError } from './errors';


export default class {
    static async getCommentsForPost(req: Request, res: Response, next: Function) {
        let postId = parseInt(req.params.postId);
        if (Number.isNaN(postId)) {
            return returnIdMustBeANumber(res);
        }

        try {
            let post = await postRepo.getPostById(postId)
            if(!post){
                return returnNotFound(res);
            }

            let comments = await repo.getCommentsForPost(postId);
            return res.send(comments);
        }  catch(error) {
            console.error(error);
            return returnServerError(res);
        } 
    };

    static async getCommentById(req: Request, res: Response, next: Function) {
        let id = parseInt(req.params.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }

        try {
            let comment = await repo.getCommentById(id)
            if(!comment){
                return returnNotFound(res);
            }

            return res.send(comment);
        }  catch(error) {
            console.error(error);
            return returnServerError(res);
        } 
    }

    static async createComment(req: Request, res: Response, next: Function) {
        if (!req.body.postId || !req.body.content || !req.body.userId ) {
            return returnInvalidRequest(res, "postId, userId, and content are required")
        }

        try {
            const post = await postRepo.getPostById(req.body.postId);
            if (!post) {
                return returnNotFound(res, "post not found");
            }

            if (req.body.parentId) {
                const parentCOmment = await repo.getCommentById(req.body.parentId);
                if (!parentCOmment) {
                    return returnNotFound(res, "parent comment not found");
                }
            }

            const newComment = new Comment(req.body.postId, req.body.content, req.body.userId, req.body.parentId);
            const successful = await repo.createComment(newComment);
            if (!successful) {
                return returnServerError(res);
            }

            return res.send(newComment);
        } catch(error) {
            console.error(error);
            return returnServerError(res);
        }
    }

    static async updateComment(req: Request, res: Response, next: Function) {
        if (!req.params.id) {
            return returnInvalidRequest(res, "id is required");
        }

        let id = parseInt(req.params.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }
        
        try {
            let comment: Comment = await repo.getCommentById(id)
            if(!comment){
                return returnNotFound(res);
            }
            
            comment.content = req.body.content;
            comment.updatedAt = new Date().toISOString();

            await repo.updateComment(comment);
            return res.send(comment);
        } catch(error) {
            console.error(error);
            return returnServerError(res);
        }
    }

    static async deleteComment(req: Request, res: Response, next: Function) {
        let id = parseInt(req.params.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }

        try {
            let comment: Comment = await repo.getCommentById(id)
            if(!comment){
                return returnNotFound(res);
            }

            await repo.deleteComment(id);
            return res.send();
        } catch(error) {
            console.error(error);
            return returnServerError(res);
        }
    }
}

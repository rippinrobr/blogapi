export const returnIdMustBeANumber = (res) => {
    return res.status(400).json({ error: 'id must be a number' });
}

export const returnInvalidRequest = (res, msg) => {
    return res.status(400).json({error: msg});
}

export const returnNotFound = (res, msg=null) => {
    if (msg) {
        return res.status(404).send(msg);
    }

    return res.status(404).send();
}

export const returnServerError = (res) => {
    return res.status(500).json({error: 'server error occurred'});
}
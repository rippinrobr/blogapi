import repo from '../repositories/post.repository';
import commentsRepo from '../repositories/comment.repository';
import { Request, Response } from 'express'
import Post from '../models/post';
import { returnIdMustBeANumber, returnInvalidRequest, returnNotFound, returnServerError } from './errors';

export default class {
    static async getAllPosts(req: Request, res: Response, next: Function) {
        try {
            let posts = await repo.getAllPosts();
            return res.send(posts);
        } catch( error ) {
            console.error(error);
            return returnServerError(res);
        }
    };

    static async getAllPostsByAuthor(req: Request, res: Response, next: Function) {
        try {
            let authorId = parseInt(req.params.authorId);
            if (Number.isNaN(authorId)) {
                return returnIdMustBeANumber(res);
            }
            
            let posts = await repo.getAllPostsByAuthorId(authorId);
            return res.send(posts);
        } catch (error) {
            console.error(error);
            return returnServerError(res);
        }
    }
    static async getPostById(req: Request, res: Response, next: Function) {
        let id = parseInt(req.params.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }

        let post = await repo.getPostById(id)
        if(!post){
            return returnNotFound(res);
        }
        return res.send(post);
    }

    static async createPost(req: Request, res: Response, next: Function) {
        if (!req.body.title || !req.body.content || !req.body.userId ) {
            return returnInvalidRequest(res, "Post title, userId, and content are required.");
        }

        const newPost = new Post(req.body.title, req.body.content, req.body.userId);
        const successful = await repo.createPost(newPost);
        if (!successful) {
            return returnServerError(res);
        }

        return res.send(newPost);
    }

    static async updatePost(req: Request, res: Response, next: Function) {
        if (!req.body.id) {
            return returnInvalidRequest(res, "id is required");
        }

        let id = parseInt(req.body.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }
        
        let post: Post = await repo.getPostById(id)
        if(!post){
            return returnNotFound(res);
        }
        
        if (req.body.title) {
            post.title = req.body.title;
        }

        if (req.body.conent) {
            post.content = req.body.content;
        }
        
        await repo.updatePost(post);
        return res.send(post);
    }

    static async deletePost(req: Request, res: Response, next: Function) {
        if (!req.params.id) {
            return returnInvalidRequest(res, 'id is required');
        }

        let id = parseInt(req.params.id);
        if (Number.isNaN(id)) {
            return returnIdMustBeANumber(res);
        }

        try {
            let deleted = await repo.deletePost(id);
            await commentsRepo.deleteCommentsForPost(id);

            return res.status(200).send();
        } catch(error) {
            console.error(error);
            return returnServerError(res);
        }
    }
}

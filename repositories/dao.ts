import * as sqlite from 'sqlite3'
const sqlite3 = sqlite.verbose();
const db = new sqlite3.Database('db/blog.db');
import * as bcrypt from 'bcrypt';
const saltRounds = 10;

export default class {

    static setupDb() {
        db.serialize(function () {
            // Drop Tables:
            const dropCommentsTable = "DROP TABLE IF EXISTS comments"
            db.run(dropCommentsTable);
            const dropUsersTable = "DROP TABLE IF EXISTS users";
            db.run(dropUsersTable);
            const dropPostsTable = "DROP TABLE IF EXISTS posts";
            db.run(dropPostsTable);

            // Create Tables:
            const createUsersTable = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT,email TEXT, password text)";
            db.run(createUsersTable);
            const createPostsTable = "CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY AUTOINCREMENT, authorId INTEGER NOT NULL, title TEXT NOT NULL, content TEXT NOT NULL, createdAt TEXT, updatedAt DATETIME)";
            db.run(createPostsTable);
            const createCommentsTable = `
               CREATE TABLE IF NOT EXISTS comments (
                   id INTEGER PRIMARY KEY AUTOINCREMENT, 
                   postId INTEGER NOT NULL, 
                   authorId INTEGER NOT NULL, 
                   content TEXT NOT NULL, 
                   parentId INTEGER,  
                   createdAt TEXT, 
                   updatedAt TEXT,
                   FOREIGN KEY(postId) REFERENCES posts(id) ON DELETE CASCADE,
                   FOREIGN KEY(parentId) REFERENCES comments(id) ON DELETE CASCADE,
                   FOREIGN KEY(authorId) REFERENCES users(id))`;
            db.run(createCommentsTable);
        });
    }

    static all(stmt, params)  {
        return new Promise((res, rej) => {
            db.all(stmt, params, (error, result) => {
                if (error) {
                    return rej(error.message);
                }
                return res(result);
            });
        })
    }

    static get(stmt, params) {
        return new Promise((res, rej) => {
            db.get(stmt, params, (error, result) => {
                if (error) {
                    return rej(error.message);
                }
                return res(result);
            });
        })
    }

    static run(stmt, params) {
        return new Promise((res, rej) => {
            db.run(stmt, params, (error, result) => {
                if (error) {
                    return rej(error.message);
                }
                return res(result);
            });
        })
    }


}

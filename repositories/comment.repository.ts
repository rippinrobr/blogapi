import dao from './dao';
import Comment from '../models/comment';
import { format } from 'path/posix';

export default class {

    static async getCommentsForPost(postId: number): Promise<Comment[]> {
        let commentsTreeQuery = `WITH RECURSIVE comments_tree(id, postId, content, authorId, parentId, createdAt, updatedAt) AS (
            SELECT c.id, c.postId, c.content, c.authorId, c.parentId, c.createdAt, c.updatedAt 
            FROM comments c 
            WHERE c.postId = ?
        
            UNION 
        
            SELECT c.id, c.postId, c.content, c.authorId, c.parentId, c.createdAt, c.updatedAt 
            FROM comments c
            JOIN comments p on p.id = c.parentId
        )
        
        SELECT * from comments_tree order by parentId, id;
        `
        const comments = await dao.all(commentsTreeQuery, [postId])
        return <Comment[]>comments;
    }

    static async getCommentById(id: number): Promise<Comment> {
        const comment = await dao.get("SELECT * FROM comments WHERE id = ?", [id])
        return <Comment>comment;
    }

    static async createComment(comment: Comment): Promise<boolean> {
        const stmt = `INSERT INTO comments (content, authorId, postId, parentId, createdAt) VALUES (?,?,?,?,?);`
        try {
            let result = await dao.run(stmt, [comment.content, comment.authorId, comment.postId, comment.parentId, comment.createdAt]);
            let resultObj = await dao.get(`SELECT id FROM comments where content = ? and authorId = ? and postId = ? and parentId = ? and createdAt = ?`, [comment.content, comment.authorId, comment.postId, comment.parentId, comment.createdAt])
            let idObj =  resultObj as {id: number};

            comment.id = idObj.id;
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    static async updateComment(comment: Comment): Promise<boolean> {
        const stmt = `UPDATE comments SET content= ?, updatedAt = ?  WHERE id = ?;`
        try {
            await dao.run(stmt, [comment.content, comment.updatedAt, comment.id]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    static async deleteCommentsForPost(postId: number) {
        let comments = await this.getCommentsForPost(postId);
        comments.forEach( (cmt) => this.deleteComment(cmt.id));
    }

    static async deleteComment(commentId: number) {
        let commentsTreeQuery = `WITH RECURSIVE comments_tree(id, parentId) AS (
            SELECT c.id, c.parentId 
            FROM comments c 
            WHERE c.id = ?
        
            UNION 
        
            SELECT c.id, c.parentId
            FROM comments c
            JOIN comments p on p.id = c.parentId
        )
        
        SELECT id, parentId from comments_tree ;
        `

        const deleteStmt = `DELETE FROM comments WHERE id = ?;`    
        try {
            const commentIds = await dao.all(commentsTreeQuery, [commentId]);
            
            let validIds = this.cleanseCommentTree(commentId, commentIds as {id:number, parentId: number}[])
            validIds.forEach(async (id) => await dao.run(deleteStmt, [id]));
           
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    private static cleanseCommentTree(commentId: number, rawIds:  {id:number, parentId: number}[]) : Array<number> {
        let validIds = new Array<number>();

        rawIds.map(obj => {
            if (validIds.indexOf(obj.parentId) > -1 || obj.id === commentId) {
                validIds.push(obj.id);
            }
        });

        return validIds;
    }
}

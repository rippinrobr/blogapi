import dao from './dao';
import Post from '../models/post';

export default class {

    static async getAllPosts(): Promise<Post[]> {
        const posts = await dao.all("SELECT * FROM posts", []);
        return <Post[]>posts;
    }

    static async getAllPostsByAuthorId(authorId: number): Promise<Post[]> {
        const posts = await dao.all("SELECT * FROM posts where authorId = ?", [authorId]);
        return <Post[]>posts;
    }

    static async getPostById(id: number): Promise<Post> {
        const post = await dao.get("SELECT * FROM posts WHERE id = ?", [id])
        return <Post>post;
    }

    static async createPost(post: Post): Promise<boolean> {
        const stmt = `INSERT INTO posts (title, content, authorId, createdAt) VALUES (?,?,?,?);`
        try {
            let result = await dao.run(stmt, [post.title, post.content, post.authorId, post.createdAt]);
            let resultObj = await dao.get(`SELECT id FROM posts where title = ? and content = ? and authorId = ? and createdAt = ?`, [post.title, post.content, post.authorId, post.createdAt])
            let idObj =  resultObj as {id: number};
            post.id = idObj.id;

            return true;
        } catch (err) {
            console.error(err);
            return false;
        }

    }

    static async updatePost(post: Post): Promise<boolean> {
        const stmt = `UPDATE posts SET title = ?, content= ?, updatedAt = ?  WHERE id = ?;`
        try {
            let updatedAt = new Date();
            await dao.run(stmt, [post.title, post.content, updatedAt.toISOString(), post.id]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    static async deletePost(postId: number) {
        const stmt = `DELETE FROM posts WHERE id = ?;`
        try {
            await dao.run(stmt, [postId]);
            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }
}

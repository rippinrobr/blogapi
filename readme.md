## Setup & Running the API server
1. run `npm install`
2. setup the database by running `npm run setupdb`
3. run `npm start` to start the service 

## Using the API

### Account and Auth
1. Create an account
```
curl --request POST \
  --url http://localhost:3000/api/auth/signup \
  --header 'Content-Type: application/json' \
  --data '{
	"email":"tester@admin.com",
	"password": "somepasswordthatislongish"
}'
```

returns 

* `200` when account is created
* `500` for any errors that occur 


2. Log in and get your token

Next, login with the email and password abouve here
```
curl --request POST \
  --url http://localhost:3000/api/auth/login \
  --header 'Content-Type: application/json' \
  --data '{
	"email":"tester@admin.com",
	"password": "somepasswordthatislongish"
}'
```

returns 

* `200` on success along with an Access-Token
* `401` when invalid credentials are used


### Post CRUD

#### CREATE Post
a `title`, `content` are required input from the user. The `authorId` and `createdAt` are set by the server
```
curl --request POST \
  --url http://localhost:3000/api/posts \
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "my post",
	"content": "this is the content"
}'
```

returns 

* `200` on success and new Post
* `400` when required fields are missing
* `500` for any other error

```
[
  {
    "id": 1,
    "authorId": 1,
    "title": "This is the Post #1",
    "content": "this is the content",
    "createdAt": "2021-12-03T02:44:56.975Z",
    "updatedAt": "2021-12-03T02:49:37.621Z"
  }
]
```


#### GET Posts

Retrieves all Posts

```
curl --request GET \
  --url http://localhost:3000/api/posts/ \
  --header 'Access-Token: <YOUR TOKEN HERE>'
```

returns 

* `200` on success and array of Posts
* `500` for any other error

```
[
  {
    "id": 1,
    "authorId": 1,
    "title": "This is the Post #1",
    "content": "this is the content",
    "createdAt": "2021-12-03T02:44:56.975Z",
    "updatedAt": "2021-12-03T02:49:37.621Z"
  }
]
```

Retreive all Posts by authorId

* `authorId` is the route parameter in the call
```
curl --request GET \
  --url http://localhost:3000/api/posts/1/author \
  --header 'Access-Token: <YOUR TOKEN HERE>'
```

Retrieve Posts by ID 
```
curl --request GET \
  --url http://localhost:3000/api/posts/12 \
  --header 'Access-Token: <YOUR TOKEN HERE>'
```

returns 

* `200` on success and array of Posts
* `400` when `authorId` is NaN
* `500` for any other error

```
[
  {
    "id": 1,
    "authorId": 1,
    "title": "This is the Post #1",
    "content": "this is the content",
    "createdAt": "2021-12-03T02:44:56.975Z",
    "updatedAt": "2021-12-03T02:49:37.621Z"
  }
]
```

#### UPDATE Post

`id` is required for updates
`title` and `content` can be updated individually or together
```
curl --request PUT \
  --url http://localhost:3000/api/posts \
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --header 'Content-Type: application/json' \
  --data '{
	"id": 1,
	"title": "This is the Post #1",
	"content": "this is the updated content"
}'
```
returns 

* `200` on success and updated Post
* `400` when `id` is NaN
* `500` for any other error

```
{
  "id": 1,
  "authorId": 1,
  "title": "This is the Post #1",
  "content": "this is the content",
  "createdAt": "2021-12-03T02:44:56.975Z",
  "updatedAt": "2021-12-03T02:46:48.393Z"
}
```

#### DELETE Post

This deletes the post the id of 1.  If the post had comments they
will also be deleted
```
curl --request DELETE \
  --url http://localhost:3000/api/posts/1`\
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --header 'Content-Type: application/json'
  ```

returns 

* `200` on success and no body
* `400` when `id` is NaN
* `404` if the `id` does not exists
* `500` for any other error

---
### Comments CRUD

#### Create Comment

This is an example of how to create a post comment
* `content` is required
* `postId` is required
```
curl --request POST \
  --url http://localhost:3000/api/comments \
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --header 'Content-Type: application/json' \
  --data '{
	"content": "This is a comment",
	"postId": 1
}'
```

returns 

* `200` on success and new Comment
* `400` when required fields aren't present
* `404` if the `postId` does not exists
* `500` for any other error

```
{
  "postId": 1,
  "content": "This is a comment",
  "authorId": 1,
  "parentId": 0,
  "createdAt": "2021-12-03T11:26:02.866Z",
  "id": 3
}
```

To create a reply to a comment use the following call

* `content` is required
* `postId` is required
* `parentId` is required

```
curl --request POST \
  --url http://localhost:3000/api/comments \
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --header 'Content-Type: application/json' \
  --data '{
	"content": "This is a comment",
	"postId": 1,
	"parentId": 3
}'
```

returns 

* `200` on success and new Comment
* `400` when required fields aren't present
* `404` if the `postId` does not exists
* `500` for any other error

```
{
  "postId": 1,
  "content": "This is a comment",
  "authorId": 1,
  "parentId": 3,
  "createdAt": "2021-12-03T11:26:02.866Z",
  "id": 4
}
```

#### GET Comments 

* `postId` route parameter is required

```
curl --request GET \
  --url http://localhost:3000/api/comments/posts/1 \
  --header 'Access-Token: <YOUR TOKEN HERE>'
```

returns an array of comments 

* `200` on success and Comments array
* `400` when `id` is NaN
* `404` if the `postId` does not exists
* `500` for any other error

```
[
  {
    "id": 1,
    "postId": 1,
    "content": "This is a comment",
    "authorId": "Rob Rowe",
    "parentId": 0,
    "createdAt": "2021-12-03T11:17:53.346Z",
    "updatedAt": null
  },
  {
    "id": 2,
    "postId": 1,
    "content": "This is a comment",
    "authorId": 1,
    "parentId": 1,
    "createdAt": "2021-12-03T11:19:41.145Z",
    "updatedAt": null
  }
  ```

Get Comment by id 

* `id` is a route parameter
```
curl --request GET \
  --url http://localhost:3000/api/comments/1 \
  --header 'Access-Token: <YOUR COMMENT HERE>'
  ```

returns comment 

* `200` and Comment
* `400` when `id` is NaN
* `404` if the `id` does not exists
* `500` for any other error

```
{
    "id": 1,
    "postId": 1,
    "content": "This is a comment",
    "authorId": "Rob Rowe",
    "parentId": 0,
    "createdAt": "2021-12-03T11:17:53.346Z",
    "updatedAt": null
}
```

#### Update Comment

* `id` is a route parameter

`content` is the only property that can be updated
```
curl --request PUT \
  --url http://localhost:3000/api/comments/1 \
  --header 'Access-Token: <YOUR TOKEN HERE>' \
  --data '{
	"content": "this is updated comment content"
}'
```

returns 

* `200` and updated Comment
* `400` when `id` is NaN
* `404` if the `id` does not exists
* `500` for any other error

```
{
  "id": 1,
  "postId": 1,
  "authorId": "1",
  "content": "this is updated comment content",
  "parentId": 0,
  "createdAt": "2021-12-03T11:17:53.346Z",
  "updatedAt": "2021-12-03T12:02:20.126Z"
}
```

#### DELETE Comment

* `id` is a route parameter

```
curl --request DELETE \
  --url http://localhost:3000/api/comments/1 \
  --header 'Access-Token: <YOUR TOKEN HERE>'
```

Returns

* `200` on success and no body
* `400` when `id` is NaN
* `404` if the `id` does not exists
* `500` for any other error
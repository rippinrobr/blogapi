import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import bodyParser from 'body-parser';
import dao from './repositories/dao';
import { authenticated, authMiddleware } from './controllers/auth.controller';
import authRoutes from './routes/auth.routes';
import commentRoutes from './routes/comment.routes';
import postsRoutes from './routes/post.routes';

const port = 3000;
export const app = express();

app.listen(port, () => console.log(`API service listening on port ${port}!`));
app.use(bodyParser.json());
app.use(authMiddleware);

app.use('/api/auth', authRoutes);
app.use('/api/posts', authenticated, postsRoutes);
app.use('/api/comments', authenticated, commentRoutes);
